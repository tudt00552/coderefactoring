<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Employees Manager</title>
        <link href="css/menu_style.css" type="text/css" rel="stylesheet" />
        <link href="css/style.css" type="text/css" rel="stylesheet" />
    </head>
    <body>
        <c:choose>
            <c:when test="${not empty sessionScope.username}">
                <div class="header">
                    <br>
                    <h1 align="center">Employees Manager</h1>
                    <div class="menu bubplastic horizontal orange">
                        <ul>
                            <li><span class="menu_r"><a href="login.jsp"><span class="menu_ar">Login</span></a></span></li>
                            <li class="highlight"><span class="menu_r"><a href="employeeManager.jsp"><span class="menu_ar">Employee Manager</span></a></span></li>
                            <li><span class="menu_r"><a href="ProcessEmployee"><span class="menu_ar">Add New Employee</span></a></span></li>
                            <li><span class="menu_r"><a href="searchEmployee.jsp"><span class="menu_ar">Search Employee</span></a></span></li>
                            <li><span class="menu_r"><a href="Login"><span class="menu_ar">logout</span></a></span></li>
                        </ul>
                        <br class="clearit" />
                    </div>
                </div>
                <div class="content">
                    <br><br>
                    <table id="tb" width="100%" align="center">
                        <tr>
                            <th colspan="11" height="50px"><h4>EMPLOYEE LIST</h4></th>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <th>Bithday Date</th>
                            <th>Hire Date</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>Country</th>
                            <th>Home Phone</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th colspan="2">Action</th>
                        </tr>
                        <jsp:useBean id="ebo" class="HRManager.bol.EmployeeBO" scope="request"/>
                        <c:choose>
                            <c:when test="${not empty param.option && not empty param.value && param.option == 'Name'}">
                                <c:forEach var="emp" items="${ebo.find(0, param.value)}">
                                    <tr>
                                        <td>
                                            ${emp.firstName} ${emp.lastName}
                                        </td>
                                        <td>${emp.birthDate}</td>
                                        <td>${emp.hireDate}</td>
                                        <td>${emp.address}</td>
                                        <td>${emp.city}</td>
                                        <td>${emp.country}</td>
                                        <td>${emp.homePhone}</td>
                                        <td>${emp.mobile}</td>
                                        <td>${emp.email}</td>
                                        <td>
                                            <a href='ProcessEmployee?action=edit&id=${emp.employeeID}'>Edit</a>
                                            <a href='ProcessEmployee?action=delete&id=${emp.employeeID}'>Delete</a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:when>
                            <c:when test="${not empty param.option && not empty param.value && param.option == 'City'}">
                                <c:forEach var="emp" items="${ebo.find(1, param.value)}">
                                    <tr>
                                        <td>
                                            ${emp.firstName} ${emp.lastName}
                                        </td>
                                        <td>${emp.birthDate}</td>
                                        <td>${emp.hireDate}</td>
                                        <td>${emp.address}</td>
                                        <td>${emp.city}</td>
                                        <td>${emp.country}</td>
                                        <td>${emp.homePhone}</td>
                                        <td>${emp.mobile}</td>
                                        <td>${emp.email}</td>
                                        <td>
                                            <a href='ProcessEmployee?action=edit&id=${emp.employeeID}'>Edit</a>
                                            <a href='ProcessEmployee?action=delete&id=${emp.employeeID}'>Delete</a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <c:forEach var="emp" items="${ebo.select()}">
                                    <tr>
                                        <td>
                                            ${emp.firstName} ${emp.lastName}
                                        </td>
                                        <td>${emp.birthDate}</td>
                                        <td>${emp.hireDate}</td>
                                        <td>${emp.address}</td>
                                        <td>${emp.city}</td>
                                        <td>${emp.country}</td>
                                        <td>${emp.homePhone}</td>
                                        <td>${emp.mobile}</td>
                                        <td>${emp.email}</td>
                                        <td>
                                            <a href='ProcessEmployee?action=Edit&id=${emp.employeeID}'>Edit</a>
                                            <a href='ProcessEmployee?action=Delete&id=${emp.employeeID}'>Delete</a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                    </table>
                    <br>
                </div>
            </c:when>
            <c:otherwise>
                <jsp:forward page="login.jsp"/>
            </c:otherwise>
        </c:choose>
    </body>
</html>